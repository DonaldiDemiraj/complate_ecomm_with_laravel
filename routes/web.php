<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FilmController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
});

Route::get('/logout', function () {
    Session::forget('user');
    return redirect('login');
});

Route::view('/register', 'register');

Route::post("/login",[UserController::class,'login']);
Route::post("/register",[UserController::class,'register']);
Route::get("/",[FilmController::class,'index']);
Route::get("detail/{id}",[FilmController::class,'detail']);
Route::get("search",[FilmController::class,'search']);
Route::post("add_to_cart",[FilmController::class,'addToCart']);
Route::get("cartlist",[FilmController::class,'cartList']);
Route::get("removecart/{id}",[FilmController::class,'removeCart']);
Route::get("ordernow",[FilmController::class,'orderNow']);
Route::post("orderplace",[FilmController::class,'orderPlace']);
Route::get("myorders",[FilmController::class,'myOrders']);

